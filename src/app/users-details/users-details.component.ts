import { Component, OnInit } from '@angular/core';
import { User, USERS } from "../Users/Users"
import {UserComponent} from "../User/user.component"

@Component({
  selector: 'app-users-details',
  templateUrl: './users-details.component.html',
  styleUrls: ['./users-details.component.css']
})
export class UsersDetailsComponent implements OnInit {
  // selectedUser: User;
  Users: User[] = USERS;
  constructor() { }
  userLink;
  show;
  ChatUSer;
  r = window.location.pathname.split("/")[2];
  t = +this.r;
  ngOnInit(){
    this.render();
  }
  render(){
    this.Users.map((node) => {
      node.id=== this.t ? 
      this.ChatUSer = node
      : null ;
    })
  };
  
  

}
