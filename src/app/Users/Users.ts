export class User {
  id: number;
  name: string;
  lastName: string;
  city: string;
  country: string;
  desc: string;
}

export const USERS: User[] = [
  { id: 1, lastName: 'Моисеев', name: 'Андрей' , city: 'Моисеев', country: 'Андрей', desc:"lorem5"},
  { id: 2, lastName: 'Апраксина', name: 'Наталия' , city: 'Апраксина', country: 'Наталия', desc:"lorem5"},
  { id: 3, lastName: 'Цызырев', name: 'Ипполит' , city: 'Цызырев', country: 'Ипполит', desc:"lore"},
  { id: 4, lastName: 'Агафонова', name: 'Валерия' , city: 'Агафонова', country: 'Валерия', desc:"lorem5"},
  { id: 5, lastName: 'Тюлепова', name: 'Ольга' , city: 'Тюлепова', country: 'Ольга', desc:"lorem"},
  { id: 6, lastName: 'Корявова', name: 'Злата' , city: 'Корявова', country: 'Злата', desc:"lorem"},
  { id: 7, lastName: 'Лобачёва', name: 'Антонина' , city: 'Лобачёва', country: 'Антонина', desc:"lorem5"},
  { id: 8, lastName: 'Жестакова', name: 'Феликс' , city: 'Жестакова', country: 'Феликс', desc:"lorem5 "},
  { id: 9, lastName: 'Агапов', name: 'Василий' , city: 'Агапов', country: 'Василий', desc:"lorem"},
  { id: 10, lastName: 'Краевский', name: 'Илья' , city: 'Краевский', country: 'Илья', desc:"lore"},
  { id: 11, lastName: 'Преображенский', name: 'Измаил' , city: 'Преображенский', country: 'Измаил', desc:"lorem"},
  { id: 12, lastName: 'Янкевич', name: 'Борис' , city: 'Янкевич', country: 'Борис', desc:"lorem"},
  { id: 13, lastName: 'Вагин', name: 'Алексей' , city: 'Вагин', country: 'Алексей', desc:"lorem"},
  { id: 14, lastName: 'Цой', name: 'Евдоким' , city: 'Цой', country: 'Евдоким', desc:"lorem5 "},
  { id: 15, lastName: 'Бершов', name: 'Ким' , city: 'Бершов', country: 'Ким', desc:"lore"},
  { id: 16, lastName: 'Шуленкова', name: 'Лада' , city: 'Шуленкова', country: 'Лада', desc:"lore"},
  { id: 17, lastName: 'Кризько', name: 'Юлия' , city: 'Кризько', country: 'Юлия', desc:"lorem5"},
  { id: 18, lastName: 'Голотина', name: 'Евгения' , city: 'Голотина', country: 'Евгения', desc:"lorem5"},
  { id: 19, lastName: 'Кручинина', name: 'Анастасия' , city: 'Кручинина', country: 'Анастасия', desc:"lorem5 "},
  { id: 20, lastName: 'Завражный', name: 'Инга' , city: 'Завражный', country: 'Инга', desc:"lore"},
];