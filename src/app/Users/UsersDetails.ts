export class UserDetails {
    id: number;
    city: string;
    country: string;
    desc: string;
  }
  
  export const USERS: UserDetails[] = [
    { id: 1, city: 'Моисеев', country: 'Андрей', desc:"lorem5" },
    { id: 2, city: 'Апраксина', country: 'Наталия', desc:"lorem5" },
    { id: 3, city: 'Цызырев', country: 'Ипполит', desc:"lore" },
    { id: 4, city: 'Агафонова', country: 'Валерия', desc:"lorem5" },
    { id: 5, city: 'Тюлепова', country: 'Ольга', desc:"lorem" },
    { id: 6, city: 'Корявова', country: 'Злата', desc:"lorem" },
    { id: 7, city: 'Лобачёва', country: 'Антонина', desc:"lorem5" },
    { id: 8, city: 'Жестакова', country: 'Феликс', desc:"lorem5 " },
    { id: 9, city: 'Агапов', country: 'Василий', desc:"lorem" },
    { id: 10, city: 'Краевский', country: 'Илья', desc:"lore" },
    { id: 11, city: 'Преображенский', country: 'Измаил', desc:"lorem" },
    { id: 12, city: 'Янкевич', country: 'Борис', desc:"lorem" },
    { id: 13, city: 'Вагин', country: 'Алексей', desc:"lorem" },
    { id: 14, city: 'Цой', country: 'Евдоким', desc:"lorem5 " },
    { id: 15, city: 'Бершов', country: 'Ким', desc:"lore" },
    { id: 16, city: 'Шуленкова', country: 'Лада', desc:"lore" },
    { id: 17, city: 'Кризько', country: 'Юлия', desc:"lorem5" },
    { id: 18, city: 'Голотина', country: 'Евгения', desc:"lorem5" },
    { id: 19, city: 'Кручинина', country: 'Анастасия', desc:"lorem5 " },
    { id: 20, city: 'Завражный', country: 'Инга', desc:"lore" },
  ];