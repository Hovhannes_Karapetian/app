import { Component } from '@angular/core';
import {USERS, User} from "./Users/Users"
import {UserComponent} from "./User/user.component"
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'test-app';
  Users = USERS;
}
