import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { MessageComponent } from './message/message.component';
import { UserComponent } from './User/user.component';
import { UsersDetailsComponent } from './users-details/users-details.component';


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home',  component: UserComponent },
  { path: 'message/:id', component: MessageComponent },
  { path: 'user/:id', component: UsersDetailsComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    MessageComponent,
    UserComponent,
    UsersDetailsComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [ RouterModule ]
})
export class AppModule { }
