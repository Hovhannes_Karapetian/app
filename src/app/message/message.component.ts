import { Component, OnInit } from '@angular/core';
import {User, USERS} from "../Users/Users"
import {UserComponent} from "../User/user.component"


export interface Message {
  send?: string;
  get?: string;
  text?: string;
}


@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {
  Users: User[] = USERS;
  MESSAGE: Message[]= [];
  name:string = '';
  constructor() { }
  
  ChatUser;
  NewMessage = "";
  r = window.location.pathname.split("/")[2];
  t = +this.r;
  ngOnInit():void {
    this.Users.map((node) => {
      node.id=== this.t ? 
      this.ChatUser = node
      : null ;
    })
  }

  AddMessage(){
    let n =this.MESSAGE.length
    this.MESSAGE[n] = {
      text: this.NewMessage,
      send: "You",
      get: this.ChatUser.name
    }
    console.log(this.MESSAGE)
  }

}
